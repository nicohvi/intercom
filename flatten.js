const assert = require('chai').assert;

function flatten (arr) {
  if(!Array.isArray(arr)) arr = [arr];
  return  arr
  .reduce((result, item) => result.concat(Array.isArray(item) ? flatten(item) : item), [])
}

const simpleArray = [1, 2, 3, 4, 5];
const nestedArray = [1, [2], [3, 4], [5]];
const deepArray = [1, [2, [3]], [[[[[[[4]]]]]]], 5];

describe('Flatten', () => {

  it('should flatten simple arrays', () => {
    assert.deepEqual(simpleArray, flatten(simpleArray));
  }); 

  it('should flatten nested arrays', () => {
    assert.deepEqual(simpleArray, flatten(nestedArray));
  });

  it('should flatten deeply nested arrays', () => {
    assert.deepEqual(simpleArray, flatten(deepArray));
  });

  it('should handle undefined', () => {
    assert.deepEqual([undefined], flatten(undefined));
  });

   it('should handle empty arrays', () => {
    assert.deepEqual([], flatten([]));
  });

})
