# Intercom test

Hiya.

This answer requires node 6 (why 6 you ask? Well, because I use
[object destructors](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment), 
which I love), and I use [mocha](https://mochajs.org) and [chai](https://chaijs.com) for testing purposes.

To run the app, simply type `node run.js`.

To run the tests, run `npm install` and `npm test`.

Oh, and to run the flatten stuff, do `npm run flatten-test`.
