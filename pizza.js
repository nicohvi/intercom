const earthRadius = 6371; // in kilometres
const { sin, cos, atan2, sqrt, PI } = Math;

const dublin = {
  lat: 53.3381985,
  lng: -6.2592576
}

function degreesToRadians (deg) {
  return deg * (PI/180);
}

// Using the Haversine formula.
function distanceFromDublin(lat, lng) {
  const deltaLatitude = degreesToRadians(dublin.lat - lat);
  const deltaLongitude = degreesToRadians(dublin.lng - lng);

  // The "bottom" cathete of the spherical triangle needed to calculate
  // length according to the Haversine formula.
  const sideA = sin(deltaLatitude/2) * sin(deltaLatitude/2) +
  cos(degreesToRadians(dublin.lat)) * cos(degreesToRadians(lat)) *
  sin(deltaLongitude/2) * sin(deltaLongitude/2);

  // The opposite cathete.
  const sideC = 2 * atan2(sqrt(sideA), sqrt(1-sideA));

  // The actual distance in terms of the Earth.
  return earthRadius * sideC;
}

function whoGetsPizza (customers) {
  return customers
  .map(customer => {
    return {
      name: customer.name,
      id: customer.user_id,
      distance: distanceFromDublin(customer.latitude, customer.longitude)
    }
  })
  .filter(data => data.distance <= 100)
  .sort((item1, item2) => item1.id >= item2.id ? 1 : -1);
 }

module.exports = {
  distance: distanceFromDublin,
  degToRad: degreesToRadians,
  pizza: whoGetsPizza
}
