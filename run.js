const app = require('./pizza');
const json = require('./customers');

app
.pizza(json)
.forEach(data => {
  console.log(`Name: ${data.name} with id: ${data.id} gets pizza!`)
});

