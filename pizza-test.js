const assert = require('chai').assert;
const app = require('./pizza');
const { degToRad, distance, pizza } = app;
const { PI, round } = Math;

const degrees = [0, 90, 180, 270, 360];
const radians = {
  0:  0,
  90: 1/2 * PI,
  180:  PI,
  270:  3/2 * PI,
  360:  2 * PI
};

// Distance as measured by me clicking on Google maps, thus we'll
// round it up to the first whole integer to ensure the numbers are
// *almost* equal (since my fingers are probably not 100% steady).
const cities = [
  { name: 'Oslo', lat: 59.915730, lng: 10.752116, distance: 1266 },
  { name: 'London', lat: 51.516596, lng: -0.126324, distance: 462 },
  { name: 'New York', lat: 40.705462, lng: -74.012352, distance: 5116 },
  { name: 'Dunboyne', lat: 53.419239, lng: -6.476198, distance: 17 }
]

const people = [
  { name: 'Nicolay', 
    latitude: cities[0].lat, 
    longitude: cities[0].lng, 
    user_id: 1 
  }, 
  { name: 'Jay-Z', 
    latitude: cities[1].lat, 
    longitude: cities[1].lng, 
    user_id: 2
  },
  { name: 'Alex Turner',
    latitude: cities[2].lat, 
    longitude: cities[2].lng, 
    user_id: 3 
  },
  { name: 'Some Irish guy',
    latitude: cities[3].lat, 
    longitude: cities[3].lng, 
    user_id: 16
  },
  { name: 'Another Irish guy',
    latitude: cities[3].lat,
    longitude: cities[3].lng,
    user_id: 4
  }
]

describe('Degrees to Radians', () => {
  it('should calculate radians from the specified degress', () => {
    degrees
    .forEach(deg => {
      assert.equal(degToRad(deg), radians[deg])
    });
  });
});

describe('Distance from Dublin', () => {
  it('should calculate distance from Dublin for the specified cities', () => {
    cities
    .forEach(city => {
      assert.equal(round(city.distance), round(distance(city.lat, city.lng)))
    });
  });
});

describe('Pizza party', () => {
  it('should only list those within 100 km', () => {
    assert.equal(2, app.pizza(people).length)
  });

  it('should sort the pizza guys according to user id ascending', () => {
    const pizzaGuys = app.pizza(people);
    assert.equal(pizzaGuys[0].user_id, people[4].id)
    assert.equal(pizzaGuys[0].name, people[4].name)
  });

  it('shows the correct name and id', () => {
    const pizzaGuy = app.pizza(people)[1];
    assert.equal(people[3].name, pizzaGuy.name) 
    assert.equal(people[3].user_id, pizzaGuy.id)
  });
  
});



