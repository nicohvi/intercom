# Proudest achievement

My actual proudest achievement is the novel I'm almost finished with,
but for the sake of this application I'll assume we're thinking of
achievements within the sphere of programming.

In this case I'd say my proudest achievement is easily my current
project, which is the implementation of a CMS-based website for a large
Norwegian university. I've been assigned the Teach Lead role, and I'm 
continously handling various aspects of development that I haven't 
encountered before, such as which technologies to leverage in order to
achieve the results we want, and how the various components of the rather
vast network of services behind the university firewall should talk
to our system.

I'm proud of this because it wasn't something I was very comfortable
with before starting the project, and I had to face up to my
responsabilities. This is also very much the case for dealing with the
project's client, though I'll readily admit that I'm more than comfortable
socially in such situations.


However, I have never been the main contact from a technical leader role,
so this was an additional challenge that I needed to face.

I feel that due to my social skills I'm able to maintain a 
positive dialogue with the client in such a way (the representative 
from the university is not technical)  that she feels comfortable with the 
solutions I propose because I explain them thoroughly and 
detail the various trade-offs and benefits each approach gives us.

While I also tremendously enjoy the programming it is the maintaining of a 
positive dialogue while constantly moving forward at a good pace that I
enjoy the most. I feel like I'm providing the backbone of a healthy IT
project, which unfortunately is rather uncommon in Norway, and that 
both parties have a common understanding not only of the technical, 
but also of the human aspects of the project.

The insights gleamed from this (ongoing) experience is that it doesn't
really matter how much you know *a priori*, but rather that you're willing
to accept your current limitations, communicate honestly, and put in the
work required to face the challenges presented to you.

By not shying away from the responsability I feel I have grown a lot, not
only as a developer/consultant, but also as a person - especially with
respect to communicating with people in a way that's thorough and honest.
